﻿//использование паттернов Builder, Facade и Observer

// Класс заметки
public class Note
{
    public string Title { get; set; }
    public string Text { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime ModifiedDate { get; set; }
}


// Интерфейс наблюдателя
public interface INoteObserver
{
    void Notify(Note note, string message);
}

// Наблюдатель для уведомления пользователя об изменениях в заметках
public class UserObserver : INoteObserver
{
    public void Notify(Note note, string message)
    {
        Console.WriteLine($"Заметка с заголовком '{note.Title}' была изменена. {message}");
    }
}

// Интерфейс строителя
public interface INoteBuilder
{
    void SetTitle(string title);
    void SetText(string text);
    
}

//Строитель для создания заметок с заданными полями
public class ConcreteNoteBuilder : INoteBuilder
{
    private Note note = new Note();

    public void SetTitle(string title)
    {
        note.Title = title;
    }

    public void SetText(string text)
    {
        note.Text = text;
    }

    public Note GetNote()
    {
        return note;
    }

}

// Класс управляющего, который использует интерфейс строителя
public class NoteDirector
{
    private INoteBuilder noteBuilder;

    public NoteDirector(INoteBuilder noteBuilder)
    {
        this.noteBuilder = noteBuilder;
    }

    public void ConstructNote()
    {
        noteBuilder.SetTitle("Заголовок заметки");
        noteBuilder.SetText("Текст заметки");
    }
}

// Класс для хранения списка наблюдателей и уведомления их разом
public class NoteSubject
{
    private List<INoteObserver> observers = new List<INoteObserver>();

    public void AddObserver(INoteObserver observer)
    {
        observers.Add(observer);
    }

    public void RemoveObserver(INoteObserver observer)
    {
        observers.Remove(observer);
    }

    public void NotifyObservers(Note note, string message)
    {
        foreach (INoteObserver observer in observers)
        {
            observer.Notify(note, message);
        }
    }
}

// Фасад для работы с заметками
public class NoteFacade : INoteObserver
{
    private List<Note> notes = new List<Note>();
    private NoteSubject noteSubject = new NoteSubject();

    public NoteFacade()
    {
        noteSubject.AddObserver(this);
    }

    public void CreateNote(Note note)
    {
        notes.Add(note);
    }

    public List<Note> GetNotes()
    {
        return notes;
    }

    public void UpdateNote(Note note)
    {
        Note existingNote = notes.FirstOrDefault(n => n.Title == note.Title);
        if (existingNote != null)
        {
            existingNote.Text = note.Text;
            existingNote.ModifiedDate = DateTime.Now;
            noteSubject.NotifyObservers(existingNote, "заметка была изменена");
        }
    }

    public void DeleteNote(Note note)
    {
        notes.Remove(note);
    }

    public void Notify(Note note, string message)
    {
        Console.WriteLine($"Заметка с заголовком '{note.Title}' {message}");
    }
}

public class Program
{
    static void Main(string[] args)
    {
        // Использование строителя для создания заметки
        ConcreteNoteBuilder noteBuilder = new ConcreteNoteBuilder();
        NoteDirector noteDirector = new NoteDirector(noteBuilder);
        noteDirector.ConstructNote();
        Note note = noteBuilder.GetNote();

        // Использование фасада для работы с заметками
        NoteFacade noteFacade = new NoteFacade();
        noteFacade.CreateNote(note);
        List<Note> notes = noteFacade.GetNotes();
        Note firstNote = notes.FirstOrDefault();
        if (firstNote != null)
        {
            Console.WriteLine($"Заголовок заметки: {firstNote.Title}");
            Console.WriteLine($"Текст заметки: {firstNote.Text}");
        }

        // Изменение заметки
        note.Text = "Новый текст заметки";
        noteFacade.UpdateNote(note);

        // Удаление заметки
        noteFacade.DeleteNote(note);

        Console.ReadLine();
    }
}